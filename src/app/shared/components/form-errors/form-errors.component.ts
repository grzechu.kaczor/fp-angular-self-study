import { Component, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'fp-form-errors',
  templateUrl: './form-errors.component.html',
  styleUrls: ['./form-errors.component.scss']
})
export class FormErrorsComponent {

  @Input() errors: ValidationErrors | undefined;
  @Input() messages: { [key: string]: string } = {};

  message: string | undefined = '';

  ngOnChanges(): void {
    let newVar = this.errors ? Object.keys(this.errors).map(x => this.messages[x]) : [];
    this.message = newVar.length > 0 ? newVar.pop() : '';
  }
}
