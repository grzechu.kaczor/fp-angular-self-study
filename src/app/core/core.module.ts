import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ContactComponent } from './components/contact-component/contact.component';
import { PageNotFoundComponent } from './components/page-not-found-component/page-not-found.component';
import { HomeComponent } from './components/home-component/home.component';
import { NavigationComponent } from './components/navigation-component/navigation.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    HomeComponent,
    NavigationComponent,
    ContactComponent,
    PageNotFoundComponent
  ],
  exports: [
    HomeComponent,
    NavigationComponent,
    ContactComponent,
    PageNotFoundComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class CoreModule {
}
