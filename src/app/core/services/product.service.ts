import { Injectable } from '@angular/core';
import { Product } from '../../models/Product';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private url = 'api/products/';

  product$: Observable<Product | undefined>;
  private productSubject: BehaviorSubject<Product | undefined>;

  products$: Observable<Product[]>;
  private productsSubject: BehaviorSubject<Product[]>;

  constructor(private httpClient: HttpClient) {
    this.productSubject = new BehaviorSubject<Product | undefined>(undefined);
    this.product$ = this.productSubject.asObservable();

    this.productsSubject = new BehaviorSubject<Product[]>([]);
    this.products$ = this.productsSubject.asObservable();
  }

  fetchProducts(): void {
    this.httpClient.get<Product[]>(this.url)
      .subscribe({
          next: result => {
            this.productsSubject.next(result);
          }
        }
      );
  }

  getProductById(productId: string): Observable<Product> {
    return this.httpClient.get<Product>(this.url + productId);
  }
}
