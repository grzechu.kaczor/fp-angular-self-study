import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from '../../models/Message';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private url = 'api/contact';

  constructor(private http: HttpClient) {
  }

  sendContactMessage(message: Message): Observable<any> {
    return this.http.post<any>(this.url, message);
  }
}
