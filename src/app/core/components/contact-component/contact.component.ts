import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactService } from '../../services/contact.service';


@Component({
  selector: 'fp-contact-component',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent {

  contactGroup: FormGroup;
  sentMessage: boolean;
  private maxlength = 200;

  emailInputMessages = {
    required: 'Email is required',
    email: 'This is not valid email',
  };
  messageInputMessages = {
    required: 'Message is required',
    maxlength: 'Message is longer than ' + this.maxlength + 'characters',
  };

  constructor(private fb: FormBuilder,
              private contactService: ContactService) {
    this.contactGroup = this.createEmptyForm();
    this.sentMessage = false;
  }

  submit() {
    this.contactService.sendContactMessage(this.contactGroup.value)
      .subscribe({
          next: () => {
            this.sentMessage = true;
          }
        }
      );
  }

  get email() {
    return this.contactGroup.get('email');
  }

  get message() {
    return this.contactGroup.get('message');
  }

  private createEmptyForm() {
    return this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      message: ['', [Validators.required, Validators.maxLength(this.maxlength)]]
    });
  }
}
