import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './core/components/home-component/home.component';
import { ContactComponent } from './core/components/contact-component/contact.component';
import { PageNotFoundComponent } from './core/components/page-not-found-component/page-not-found.component';

const routes: Routes = [
  { path: 'contact', component: ContactComponent },
  {
    path: 'products',
    loadChildren: () => import('./products/products.module').then(m => m.ProductsModule)
  },
  { path: '', component: HomeComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
