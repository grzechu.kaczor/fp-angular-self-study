import { Component, OnInit } from '@angular/core';
import { Product } from '../../../models/Product';
import { Router } from '@angular/router';
import { ProductService } from '../../../core/services/product.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'fp-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  products$: Observable<Product[]>;

  constructor(private router: Router,
              private productService: ProductService) {
    this.products$ = productService.products$;
  }

  ngOnInit(): void {
    this.productService.fetchProducts();
  }

  existProductToShow(length: number): boolean {
    return length > 0;
  }

  checkPrice(price: number): boolean {
    return price > 0 && price !== 0;
  }

  redirectToProductDetails(productId?: string) {
    this.router.navigateByUrl('/products/' + productId);
  }
}
