import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../../../core/services/product.service';
import { Observable } from 'rxjs';
import { Product } from '../../../models/Product';

@Component({
  selector: 'fp-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  productId: string | null;
  product$: Observable<Product | undefined>;

  constructor(private route: ActivatedRoute,
              private productService: ProductService) {
    this.productId = this.route.snapshot.paramMap.get('id');
    this.product$ = new Observable<Product | undefined>();
  }

  ngOnInit(): void {
    if (this.productId) {
      this.product$ = this.productService.getProductById(this.productId);
    }
  }
}
